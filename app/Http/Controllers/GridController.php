<?php

namespace App\Http\Controllers;

use App\Http\Repositories\GridRepository;
use App\Http\Repositories\GridValueRepository;
use Illuminate\Http\Request;

class GridController extends Controller
{
    private $gridRepository,$gridValueRepository;

    public function __construct(GridValueRepository $gridValueRepository, GridRepository $gridRepository)
    {
        $this->gridRepository = $gridRepository;
        $this->gridValueRepository = $gridValueRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('index'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $grid = $this->gridRepository->createGrid($input);
        $grid_values = $this->gridValueRepository->getGridValues($grid->id);

        return response()->json([
            'status' => true,
            'grid_values'    => $grid_values,
            'grid_id' => $grid->id
        ], 200);
    }

    
}
