<?php

namespace App\Http\Controllers;

use App\Http\Repositories\GridValueRepository;
use Illuminate\Http\Request;

class GridValueController extends Controller
{
    private $gridValueRepository;

    public function __construct(GridValueRepository $gridValueRepository)
    {
        $this->gridValueRepository = $gridValueRepository;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('index'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $grid_value = $this->gridValueRepository->createGridValue($input);
        
        return response()->json([
            'status' => true,
            'grid_value'    => $grid_value,
        ], 200);
    }
}
