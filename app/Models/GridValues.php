<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GridValues extends Model
{
    use HasFactory;
    protected $table = "grid_values";
    protected $fillable = [
    					'grid_id',
    					'row_no',
    					'column_no',
    					'item_name',
    					'price'
    				];
}
