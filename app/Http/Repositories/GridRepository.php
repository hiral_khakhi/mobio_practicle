<?php

namespace App\Http\Repositories;

use App\Models\Grid;
use App\Models\GridValues;
use Illuminate\Http\Request;

class GridRepository
{
	public function createGrid($input)
	{
		$grid = Grid::where('rows',$input['rows'])
                ->where('columns',$input['columns'])
                ->first();
        
        if(!$grid){
            $grid = Grid::create([
                'rows'=>$input['rows'],
                'columns'=>$input['columns']
            ]);
        }
        
        return $grid;
	}

}