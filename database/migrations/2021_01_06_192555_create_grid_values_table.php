<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGridValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grid_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('grid_id');
            $table->tinyInteger('row_no');
            $table->tinyInteger('column_no');
            $table->string('item_name');
            $table->double('price');
            $table->timestamps();
            $table->foreign('grid_id')->references('id')->on('grid')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grid_values');
    }
}
