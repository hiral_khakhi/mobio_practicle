<?php

namespace App\Http\Repositories;

use App\Models\Grid;
use App\Models\GridValues;
use Illuminate\Http\Request;

class GridValueRepository
{
	public function createGridValue($input)
	{
		$grid_value = GridValues::updateOrCreate(['grid_id'=>$input['grid_id'],'row_no'=>$input['row_no'],'column_no'=>$input['column_no']],['item_name'=>$input['item_name'],'price'=>$input['price']]);
        
        return $grid_value;
	}

	public function getGridValues($grid_id)
	{
		$grid_values = GridValues::where('grid_id',$grid_id)->get();
		return $grid_values;
	}

}