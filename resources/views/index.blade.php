<!DOCTYPE html>
<html>
<head>
	<title>Order System</title>

	<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

	<!-- Bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	<!-- Font css -->
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

	<script type="text/javascript" src="js/app.js"></script>
	
</head>
<body>
	<div id="app">
		<div class="container">
	       <div class="row">
	           <div class="col-md-12">
	              <h2 class="text-center">Order System</h2>
	              <div class="row">
	                <div class="col-md-offset-5 col-md-2">
	                    <div class="form-group">
	                      <label class="control-label">Row Number</label>
	                      <input type="number" name="rows" class="form-control" v-model.number="grid.rows">
	                      <span class="text-danger" v-if="errors.rows">@{{errors.rows }}</span>
	                    </div>
	                    <div class="form-group">
	                      <label class="control-label">Column Number</label>
	                      <input type="number" name="columns" class="form-control"  v-model.number="grid.columns">
	                      <span class="text-danger" v-if="errors.columns">@{{ errors.columns }}</span>
	                    </div>
	                    <div class="form-group">
	                      <center><button class="btn btn-primary" v-on:click="createGrid">Submit</button></center>
	                    </div>
	                </div>
	              </div>
	           </div>
	       </div>
	       <div class="row">
	       	<div v-if="current_rows">
	       		<center>
	       		<div class="row" v-for="(row,row_no) in current_rows">
	       			
	       			<button class="btn btn-default col-md-1" v-for="(column,column_no) in current_columns" v-on:click="createOrder(row_no,column_no)">

	       				<span v-if="get_element(row_no,column_no)">@{{get_element(row_no,column_no).item_name }}</span>
			    		<i class="fa fa-plus" v-else></i>
					</button>

				</div>
				</center>
			</div>
	       	
	       </div>
	   </div>

	   <!-- Order Create Modal -->
	   <div class="modal fade" tabindex="-1" role="dialog" id="create_order_modal">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                               aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title">Create Order</h4>
                   </div>
                   <div class="modal-body">
                       <div class="alert alert-danger" v-if="errors.length > 0">
                           <ul>
                               <li v-for="error in errors">@{{ error }}</li>
                           </ul>
                       </div>
                       <div class="form-group">
                           <label for="name">Item Name:</label>
                           <input type="text" name="item_name" id="name"  class="form-control"
                                  v-model="grid_value.item_name">
                            <span class="text-danger" v-if="errors.item_name">@{{ errors.item_name }}</span>
                       </div>
                       <div class="form-group">
                           <label for="price">Price:</label>
                           <input type="number" name="price" id="price"  class="form-control"
                                  v-model.number="grid_value.price">
                            <span class="text-danger" v-if="errors.price">@{{ errors.price }}</span>
                       </div>
                   </div>
                   <div class="modal-footer">
                       <button @click="storeOrder" class="btn btn-primary">Submit</button>
                   </div>
               </div><!-- /.modal-content -->
           </div><!-- /.modal-dialog -->
       </div><!-- /.modal -->
	</div>
</body>
<!-- Bootstrap js -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script type="text/javascript">
	
	const app = new Vue({
    	el: '#app',
    	data: {           
           	grid_id: '',
          	grid: {
            	rows: '',
            	columns: ''
          	},
           	grid_value: {
            	item_name: '',
               price: ''
           	},
           	errors: {
           		rows: '',
           		columns: '',
           		item_name: '',
           		price: ''
           	},
           	grid_values: '', // array of all available grid items
       		current_rows : '', // no. of rows in current grid
       		current_columns : '', //no. of columns in current grid
       		temp_row_no:'', // row no. of currently clicked grid item
       		temp_column_no:'', // column no. of currently clicked grid item
       	},
    	methods: {
      		createGrid: function()
      		{
      			is_valid = true;
      			//check if Row number is valid
      			if(!this.grid.rows || this.grid.rows < 1 || this.grid.rows > 12 ){
      				this.errors.rows = 'Please enter valid number';
      				is_valid = false;
      			}else{
      				this.errors.rows = '';
      			}

      			//check if Column numberis valid
      			if(!this.grid.columns || this.grid.columns < 1 || this.grid.columns > 12 ){
      				this.errors.columns = 'Please enter valid number';	
      				is_valid = false;
      			}else{
      				this.errors.column = '';
      			}

      			// create grid if no errors
      			if(is_valid){
      				this.errors.rows = '';
      				this.errors.columns = '';
      			
	               axios.post('/api/grid/store', {
	                   rows: this.grid.rows,
	                   columns: this.grid.columns,
	               })
	               .then(response => {
	               		//store o of rows and column for future use
	                   this.current_rows = this.grid.rows;
	                   this.current_columns = this.grid.columns;

	                   //empty the grid before inserting new grid values
	                   this.grid_values = [];

	                   if(response.data.grid_values){
	                   	this.grid_values = response.data.grid_values;
	                   }
	                   
	                   //store grid_id o be used while create order
	                   this.grid_id = response.data.grid_id;
	               });
	            }
      		},
      		createOrder: function(row_no,column_no)
      		{
		      	this.temp_column_no = column_no;
		      	this.temp_row_no = row_no;
		      	if(this.get_element(row_no,column_no)){
		      		this.grid_value.item_name = this.get_element(row_no,column_no).item_name;
		      		this.grid_value.price = this.get_element(row_no,column_no).price;
		      	}
		      	$("#create_order_modal").modal("show");
	     	},
      		storeOrder()
      		{
      			is_valid = true;
      			if(!this.grid_value.item_name){
      				this.errors.item_name = "Please enter item name";
      				is_valid = false;
      			}else{
      				this.errors.item_name = "";
      			}
      			if(!this.grid_value.price){
      				this.errors.price = "Please enter price";
      				is_valid = false;
      			}else{
      				this.errors.price = "";
      			}
      			if(is_valid){
		      	 	$("#create_order_modal").modal("hide");
		      	 	axios.post('/api/grid_value/store', {
				 		row_no: this.temp_row_no,
				 		column_no: this.temp_column_no,
			           	item_name: this.grid_value.item_name,
			           	price: this.grid_value.price,
			           	grid_id: this.grid_id
			       	})
			       	.then(response => {
			       		if(this.get_element(this.temp_row_no,this.temp_column_no)){
			       			index = this.grid_values.findIndex(x => x.column_no == this.temp_column_no && x.row_no == this.temp_row_no);
			       			this.grid_values[index].item_name = response.data.grid_value.item_name;
			       			this.grid_values[index].price = response.data.grid_value.price;
			       		}else{
			           		this.grid_values.push(response.data.grid_value);
			           	}
			       	})
			    }
      		},

		    get_element(row_no2,column_no2)
		    {
		      	return this.grid_values.find(function(element){
		      		return element.row_no == row_no2 && element.column_no == column_no2;
		      	});
		    },
		    reset_modal()
		    {
		    	this.grid_value.item_name = "";
		        this.grid_value.price = "";
		    }
    	}
	});
	
	$('#create_order_modal').on('hidden.bs.modal', function (e) {
  		app.reset_modal();	
	})

</script>
</html>